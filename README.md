# cadfael-cli

The global command line interface (CLI) runner for [cadfael](https://github.com/automatonic/cadfael)

## Getting Started
First install the module globally with: `npm install -g cadfael-cli`

Then run one of the `monk` CLI tools from your prompt:

 * `monk` - to get an overview of tools
 * `monk observe` - to observe [precepts](https://github.com/automatonic/cadfael/wiki/precepts.js) in the current directory
 * `monk found` - to scaffold new projects
 * `monk info` - to get diagnostics about the cli script and the effective cadfael library
 * `monk help [tool]` - to list options and further tool details

In order to run, the `cadfael-cli` package depends on a [cadfael](https://github.com/automatonic/cadfael) package being installed. 
You can do this manually in a directory using `npm install cadfael` or by using the `found` tool.

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using cadfael and the provided `precepts.js` file.

## Release History
 * 0.1.0 - Basic CLI features are working

## License
Copyright (c) 2013 Elliott B. Edwards  
Licensed under the MIT license.
