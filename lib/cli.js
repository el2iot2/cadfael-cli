/*
 * cadfael-cli
 * https://github.com/automatonic/cadfael-cli
 *
 * Copyright (c) 2013 Elliott B. Edwards
 * Licensed under the MIT license.
 */

var when = require('when');
var spawn = require('child_process').spawn;
var findup = require('findup-sync');
var log = require('logule').init(module);

function loadCadfaelAsync()
{
  var deferred = when.defer();
  var child = spawn(process.execPath, ['-p', '-e', 'require.resolve("cadfael")']);
  var effectivePath = '';
  child.stdout.on('data', function(data) { effectivePath += data; });
  child.on('exit', function(code) {
    // Removing trailing newline from stdout.
    effectivePath = effectivePath.trim();
    // If a local cadfael isn't found, we might be observing cadfael itself.
    // Check for cadfael's lib/cadfael.js file.
    if (code !== 0) {
      log.trace('checking for observe of cadfael itself');
      effectivePath = findup('lib/cadfael.js');
      // No cadfael install found!
      if (!effectivePath) {
        deferred.reject(new Error('Unable to find local cadfael package.'));
        return;
      }
    }
    // Fulfill with local cadfael
    try {
      var cadfael = require(effectivePath);
      cadfael.effectivePath = effectivePath;
      deferred.resolve(cadfael);
    }
    catch (error) {
      deferred.reject(error);
    }
  });
  return deferred.promise;
}

function version() {
  return require('../package.json').version;
}
//Gets the current version of the library
exports.version = version;

function logInfoSimple(pkg, key, value) {
  console.log(value);
}

function logInfoDefault(pkg, key, value) {
  log.sub(pkg).sub(key).info(value);
}

function processExitWithError(error) {
  log.error(error.message);
  process.exit(1); //failure
}

exports.info = function(simple, libVersion, libPath, cliVersion, cliPath) {
  return loadCadfaelAsync()
    .then(function(cadfael) {
      var logInfo = simple ? logInfoSimple : logInfoDefault;
      //if no flag is specified, display all
      var all = !(libVersion||libPath||cliVersion||cliPath);
      if (libVersion || all) {
        logInfo('cadfael', 'version', cadfael.version());
      }
      if (libPath || all) {
        logInfo('cadfael', 'path', cadfael.effectivePath);
      }
      if (cliVersion || all) {
        logInfo('cadfael-cli', 'version', version());
      }
      if (cliPath || all) {
        logInfo('cadfael-cli', 'path', __filename);
      }
      process.exit(0); //success
    },
    processExitWithError);
};

exports.observe = function(preceptFile, precept) {
  return loadCadfaelAsync()
    .then(function(cadfael) {
      cadfael
        .rootObserve(preceptFile, precept)
        .then(function() {
          process.exit(0); //success
        },
        processExitWithError);
    },
    processExitWithError);
};
